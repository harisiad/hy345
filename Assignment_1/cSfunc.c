#include "cSsh.h"

int isDaemon = NODAEMON;
int _state = NORM;

char *cmd_funcs[]={
	"cd",
	"set",
	"unset",
	"printlvars",
	"exit"
};

void cmd_create(){
	char *usD;
	struct passwd *uname;
	char buf[PATH_MAX+1];
	
	uname = getpwuid(getuid());
	usD = getcwd(buf,PATH_MAX+1);
	
	if(uname == NULL){
		printf("There was an error retrieving the login...\n");
		exit(1);
	}
	
	printf("%s@cs345sh%s$ ",uname->pw_name,usD);
	
	return;
}

char** cmd_parser(){
	int p=0, i=0;
	int countRed = 0;
	char *input;
	char *tokens;
	char **cmd_toks;
	char ch;
	
	input = (char*)malloc(sizeof(char)*(PATH_MAX+1));
	_state = NORM;
	while(1){
		
		ch = getchar();
		
		if((ch == '\0') || (ch == '\n')){
			input[p] = '\0';
			break;
		} else if(ch == '='){
			input[p] = ' ';
			p++;
		} else if(ch == '<'){
			_state = INRED;
			input[p] = ' ';
			p++;
		} else if(ch == '>'){
			countRed++;
			if(_state == INRED){
				_state = INOUTRED;
			} else {
				_state = OUTREDWR;
				if(countRed == 2){
					_state = OUTREDA;
					countRed=0;
				}
			}
			input[p] = ' ';
			p++;
			
		} else {
			input[p] = ch;
			p++;
		}
		
	}
	
	if(input[p-1] == '&'){
		isDaemon = DAEMON;
		input[p-1] = '\0';
	}
	
	tokens = strtok(input," \n\t");
	
	cmd_toks = (char**)malloc(sizeof(char*) * 6);
	
	while(tokens){
		cmd_toks[i] = (char*)malloc((strlen(tokens)+1) * sizeof(char));
		strcpy(cmd_toks[i],tokens);
		strcat(cmd_toks[i++],"\0");
		tokens = strtok(NULL," ");
	}
	cmd_toks[i] = "\0";
	return cmd_toks;
}

int cmd_process(char** cmd_line){
	
	pid_t pid, wpid;
	
	int status,i;
	int ret = 1;
	int args = 0;
	int tmpargs = 0;
	int countinout = 0;
	
	/*Vars for commands and parameters*/
	char *envp[6];
	
	char *command;
	char **parameters ;
	/*Temp Vars for commands and parameters used in Variables*/
	char *tmpEnvp[6];
	
	char *tmpCommand;
	char **tmpParameters;
	
	/*Files for Redirection States of the shell*/
	
	int fin, fout;
	char *fileName;
	char *outFileName;
	
	/*-----------------------------------------------------------------------------*/
	/*-------------------------- STATE = NORMAL CALL ------------------------------*/
	/*-----------------------------------------------------------------------------*/
	if(_state == NORM){
		/*------------------------------------*/
		/*Allocation of Command and Parameters*/
		command = (char*)malloc(strlen(cmd_line[0]) * sizeof(char));
		if(command == NULL){
			printf("Failed to allocate memory for command char*.. Programm exiting...");
			exit(1);
		}
		args = 0;
		for(i=1; cmd_line[i] != "\0"; i++){
			args++;
		}
		args++;
		
		parameters = (char**)malloc(sizeof(char*) * args);
		if(parameters == NULL){
			printf("Failed to allocate memory for parameters char*.. Programm exiting...");
			exit(1);
		}
		/*---------------------------------------------*/
		/*Dividing command line from input to 
		 * Command and Parameters in different variables
		 **/
		 
		command = cmd_line[0];
		strcat(command, "\0");
		
		envp[0] = command;
		if(cmd_line[1] != "\0"){
			parameters[0] = (char*)malloc(sizeof(char)*strlen(cmd_line[1]));
			parameters[0] = cmd_line[1];
			envp[1] = cmd_line[1];
			for(i=2; cmd_line[i] != NULL; i++){
				parameters[i-1] = (char*)malloc(sizeof(char)*strlen(cmd_line[i]));
				parameters[i-1] = cmd_line[i];
				
				envp[i] = cmd_line[i];
			}
			strcat(parameters[i],"\0");
			envp[i-1] = (char*)0;
		} else {
			envp[1] = (char*)0;
			parameters[0] = command;
		}
	
		/*---------------------------------*/
		/*Choosing a different approach regarding which command has been inserted
		 * For ret != 2 build in system functions
		 * Else system functions of UNIX using fork and exec
		 **/
		
		ret = cmd_switch(command,parameters,args);
		
		if(ret != 2){
			return ret;
		} else {
			
			pid = fork();
			if(pid < 0){
				printf("There was an error with fork, system is exiting...");
				exit(1);
			} else if(pid == 0) {/*Child Process*/
				if(execvp(cmd_line[0],envp) == -1){
					perror("cmd");
				}
				
				exit(EXIT_FAILURE);
				
			} else { /*Father Process*/
				
				if(_state != DAEMON){
					do{
						wpid = waitpid(pid,&status,WUNTRACED);
					}while(!WIFEXITED(status) && !WIFSIGNALED(status));
				}
			}
		}
	/*-----------------------------------------------------------------------------*/
	/*--------------------------- STATE = TEMP ------------------------------------*/
	/*-----------------------------------------------------------------------------*/
	} else if (_state == TEMP) {
		/*------------------------------------*/
		/*Allocation of Command and Parameters*/
		tmpCommand = (char*)malloc((strlen(cmd_line[0]) * sizeof(char)) + 1);
		if(tmpCommand == NULL){
			printf("Failed to allocate memory for command char*.. Programm exiting...");
			exit(1);
		}
		tmpargs = 0;
		for(i=1; cmd_line[i] != '\0'; i++){
			tmpargs += strlen(cmd_line[i]);
		}
		
		
		tmpParameters = (char**)malloc((sizeof(char*) * tmpargs) + 1);
		if(tmpParameters == NULL){
			printf("Failed to allocate memory for parameters char*.. Programm exiting...");
			exit(1);
		}
		/*---------------------------------------------*/
		/*Dividing command line from input to 
		 * Command and Parameters in different variables
		 **/
		 
		tmpCommand = cmd_line[0];
		strcat(tmpCommand, "\0");
		
		tmpEnvp[0] = tmpCommand;
		if(cmd_line[1] != "\0"){
			
			tmpParameters[0] = (char*)malloc((sizeof(char)*strlen(cmd_line[1])) + 1);
			tmpParameters[0] = cmd_line[1];
			
			tmpEnvp[1] = cmd_line[1];
			
			for(i=2; cmd_line[i] != NULL; i++){
				tmpParameters[i-1] = (char*)malloc((sizeof(char)*strlen(cmd_line[i])) + 1);
				tmpParameters[i-1] = cmd_line[i];
				
				tmpEnvp[i] = cmd_line[i];
			}
			strcat(tmpParameters[i],"\0");
			tmpEnvp[i-1] = (char*)0;
		} else {
			tmpEnvp[1] = (char*)0;
			tmpParameters[0] = tmpCommand;
		}
		/*---------------------------------*/
		/*Choosing a different approach regarding which command has been inserted
		 * For ret != 2 build in system functions
		 * Else system functions of UNIX using fork and exec
		 **/

		ret = cmd_switch(tmpCommand,tmpParameters,tmpargs);

		
		if(ret != 2){
			return ret;
		} else {
			
			pid = fork();
			if(pid < 0){
				printf("There was an error with fork, system is exiting...");
				exit(1);
			} else if(pid == 0) {/*Child Process*/
				if(execvp(cmd_line[0],tmpEnvp) == -1){
					perror("cmd");
				}
				
				exit(EXIT_FAILURE);
				
			} else { /*Father Process*/
				
				if(_state != DAEMON){
					do{
						wpid = waitpid(pid,&status,WUNTRACED);
					}while(!WIFEXITED(status) && !WIFSIGNALED(status));
				}
			}
		}
	/*-----------------------------------------------------------------------------*/
	/*---------------------- STATE = INPUT REDIRECTION ----------------------------*/
	/*-----------------------------------------------------------------------------*/
	} else if(_state == INRED) {
		/*------------------------------------*/
		/*Allocation of Command and Parameters*/
		command = (char*)malloc(strlen(cmd_line[0]) * sizeof(char));
		if(command == NULL){
			printf("Failed to allocate memory for command char*.. Programm exiting...");
			exit(1);
		}
		args = 0;
		for(i=1; cmd_line[i] != "\0"; i++){
			args++;
		}
		args++;
		
		parameters = (char**)malloc(sizeof(char*) * args);
		if(parameters == NULL){
			printf("Failed to allocate memory for parameters char*.. Programm exiting...");
			exit(1);
		}
		/*---------------------------------------------*/
		/*Dividing command line from input to 
		 * Command and Parameters in different variables
		 **/
		 
		command = cmd_line[0];
		strcat(command, "\0");
		
		envp[0] = command;
		if(cmd_line[1] != "\0"){
			parameters[0] = (char*)malloc(sizeof(char)*strlen(cmd_line[1]));
			parameters[0] = cmd_line[1];
			
			envp[1] = cmd_line[1];
			
			for(i=2; cmd_line[i] != NULL; i++){
				if((strchr(cmd_line[i], '.')) != NULL){
					fileName = (char*)malloc(strlen(cmd_line[i])*sizeof(char) + 1);
					strcpy(fileName,cmd_line[i]);
				} else {
					envp[i] = cmd_line[i];
					parameters[i-1] = (char*)malloc(sizeof(char)*strlen(cmd_line[i]));
					parameters[i-1] = cmd_line[i];
				}
				
				
			}
			strcat(parameters[i],"\0");
			envp[i-1] = (char*)0;
		} else {
			envp[1] = (char*)0;
			parameters[0] = command;
		}
		
		fin = open(fileName, O_RDONLY, 0644);
		
		pid = fork();
		if(pid < 0){
			printf("There was an error with fork, system is exiting...");
			exit(1);
		} else if(pid == 0) {/*Child Process*/
			
			dup2(fin,STDIN_FILENO);
			close(fin);
			if(execvp(cmd_line[0],envp) == -1){
				perror("cmd");
			}
			exit(EXIT_FAILURE);
			
		} else { /*Father Process*/
			
			if(_state != DAEMON){
				do{
					wpid = waitpid(pid,&status,WUNTRACED);
				}while(!WIFEXITED(status) && !WIFSIGNALED(status));
			}
		}
	/*-----------------------------------------------------------------------------*/
	/*-------------------- STATE = OUTPUT WRITE REDIRECTION -----------------------*/
	/*-----------------------------------------------------------------------------*/
	} else if(_state == OUTREDWR){
		
		/*------------------------------------*/
		/*Allocation of Command and Parameters*/
		command = (char*)malloc(strlen(cmd_line[0]) * sizeof(char));
		if(command == NULL){
			printf("Failed to allocate memory for command char*.. Programm exiting...");
			exit(1);
		}
		args = 0;
		for(i=1; cmd_line[i] != "\0"; i++){
			args++;
		}
		args++;
		
		parameters = (char**)malloc(sizeof(char*) * args);
		if(parameters == NULL){
			printf("Failed to allocate memory for parameters char*.. Programm exiting...");
			exit(1);
		}
		/*---------------------------------------------*/
		/*Dividing command line from input to 
		 * Command and Parameters in different variables
		 **/
		 
		command = cmd_line[0];
		strcat(command, "\0");
		
		envp[0] = command;
		if(cmd_line[1] != "\0"){
			parameters[0] = (char*)malloc(sizeof(char)*strlen(cmd_line[1]));
			parameters[0] = cmd_line[1];
			
			envp[1] = cmd_line[1];
			
			for(i=2; cmd_line[i] != NULL; i++){
				if((strchr(cmd_line[i], '.')) != NULL){
					fileName = (char*)malloc(strlen(cmd_line[i])*sizeof(char) + 1);
					strcpy(fileName,cmd_line[i]);
				} else {
					envp[i] = cmd_line[i];
				}
				parameters[i-1] = (char*)malloc(sizeof(char)*strlen(cmd_line[i]));
				parameters[i-1] = cmd_line[i];
				
			}
			strcat(parameters[i],"\0");
			envp[i-1] = (char*)0;
		} else {
			envp[1] = (char*)0;
			parameters[0] = command;
		}
		
		fout = open(fileName, O_CREAT|O_TRUNC|O_WRONLY, 0644);
		
		pid = fork();
		if(pid < 0){
			printf("There was an error with fork, system is exiting...");
			exit(1);
		} else if(pid == 0) {/*Child Process*/
			
			dup2(fout,STDOUT_FILENO);
			close(fout);
			if(execvp(cmd_line[0],envp) == -1){
				perror("cmd");
			}
			exit(EXIT_FAILURE);
			
		} else { /*Father Process*/
			
			if(_state != DAEMON){
				do{
					wpid = waitpid(pid,&status,WUNTRACED);
				}while(!WIFEXITED(status) && !WIFSIGNALED(status));
			}
		}
	/*-----------------------------------------------------------------------------*/
	/*-------------------- STATE = INPUT OUTPUT REDIRECTION -----------------------*/
	/*-----------------------------------------------------------------------------*/	
	} else if(_state == INOUTRED) {
		/*------------------------------------*/
		/*Allocation of Command and Parameters*/
		command = (char*)malloc(strlen(cmd_line[0]) * sizeof(char));
		if(command == NULL){
			printf("Failed to allocate memory for command char*.. Programm exiting...");
			exit(1);
		}
		args = 0;
		for(i=1; cmd_line[i] != "\0"; i++){
			args++;
		}
		args++;
		
		parameters = (char**)malloc(sizeof(char*) * args);
		if(parameters == NULL){
			printf("Failed to allocate memory for parameters char*.. Programm exiting...");
			exit(1);
		}
		/*---------------------------------------------*/
		/*Dividing command line from input to 
		 * Command and Parameters in different variables
		 **/
		 
		command = cmd_line[0];
		strcat(command, "\0");
		
		envp[0] = command;
		if(cmd_line[1] != "\0"){
			parameters[0] = (char*)malloc(sizeof(char)*strlen(cmd_line[1]));
			parameters[0] = cmd_line[1];
			envp[1] = cmd_line[1];
			for(i=2; cmd_line[i] != NULL; i++){
				if((strchr(cmd_line[i],'.')) == NULL) {
					envp[i] = cmd_line[i];
					parameters[i-1] = (char*)malloc(sizeof(char)*strlen(cmd_line[i]));
					parameters[i-1] = cmd_line[i];
				} else {
					if(countinout == 0){
						fileName = (char*)malloc(strlen(cmd_line[i])*sizeof(char) + 1);
						strcpy(fileName, cmd_line[i]);
						countinout++;
					} else {
						outFileName = (char*)malloc(strlen(cmd_line[i])*sizeof(char) + 1);
						strcpy(outFileName, cmd_line[i]);
						countinout++;
					}
					
				}
			}
			countinout = 0;
			strcat(parameters[i],"\0");
			envp[i-1] = (char*)0;
		} else {
			envp[1] = (char*)0;
			parameters[0] = command;
		}
		
		fin = open(fileName, O_RDONLY, 0644);
		fout = open(outFileName, O_CREAT|O_TRUNC|O_WRONLY, 0644);
		
		pid = fork();
		if(pid < 0){
			printf("There was an error with fork, system is exiting...");
			exit(1);
		} else if(pid == 0) {/*Child Process*/
			
			dup2(fin,STDIN_FILENO);
			dup2(fout, STDOUT_FILENO);
			
			if(execvp(cmd_line[0],envp) == -1){
				perror("cmd");
			}
			exit(EXIT_FAILURE);
			
		} else { /*Father Process*/
			
			if(_state != DAEMON){
				do{
					wpid = waitpid(pid,&status,WUNTRACED);
				}while(!WIFEXITED(status) && !WIFSIGNALED(status));
			}
		}
		
	/*-----------------------------------------------------------------------------*/
	/*-------------------- STATE = OUTPUT APPEND REDIRECTION -----------------------*/
	/*-----------------------------------------------------------------------------*/	
	} else {
		/*------------------------------------*/
		/*Allocation of Command and Parameters*/
		command = (char*)malloc(strlen(cmd_line[0]) * sizeof(char));
		if(command == NULL){
			printf("Failed to allocate memory for command char*.. Programm exiting...");
			exit(1);
		}
		args = 0;
		for(i=1; cmd_line[i] != "\0"; i++){
			args++;
		}
		args++;
		
		parameters = (char**)malloc(sizeof(char*) * args);
		if(parameters == NULL){
			printf("Failed to allocate memory for parameters char*.. Programm exiting...");
			exit(1);
		}
		/*---------------------------------------------*/
		/*Dividing command line from input to 
		 * Command and Parameters in different variables
		 **/
		 
		command = cmd_line[0];
		strcat(command, "\0");
		
		envp[0] = command;
		if(cmd_line[1] != "\0"){
			parameters[0] = (char*)malloc(sizeof(char)*strlen(cmd_line[1]));
			parameters[0] = cmd_line[1];
			envp[1] = cmd_line[1];
			for(i=2; cmd_line[i] != NULL; i++){
				if((strchr(cmd_line[i], '.')) != NULL){
					fileName = (char*)malloc(strlen(cmd_line[i])*sizeof(char) + 1);
					strcpy(fileName,cmd_line[i]);
				} else {
					envp[i] = cmd_line[i];
				}
				parameters[i-1] = (char*)malloc(sizeof(char)*strlen(cmd_line[i]));
				parameters[i-1] = cmd_line[i];
				
			}
			strcat(parameters[i],"\0");
			envp[i-1] = (char*)0;
		} else {
			envp[1] = (char*)0;
			parameters[0] = command;
		}
		
		fout = open(fileName, O_CREAT|O_RDWR|O_APPEND, 0644);
		
		pid = fork();
		if(pid < 0){
			printf("There was an error with fork, system is exiting...");
			exit(1);
		} else if(pid == 0) {/*Child Process*/
			
			dup2(fout,1);
			close(fout);
			if(execvp(cmd_line[0],envp) == -1){
				perror("cmd");
			}
			exit(EXIT_FAILURE);
			
		} else { /*Father Process*/
			
			if(_state != DAEMON){
				do{
					wpid = waitpid(pid,&status,WUNTRACED);
				}while(!WIFEXITED(status) && !WIFSIGNALED(status));
			}
		}
	}
	
	return ret;
}

/*----------------------------------*/
/*Choosing between build in commands*/
int cmd_switch(char* coms, char** params, int ars){
	int ret = 1;
	char **tmp;
	
	if(strcmp(coms,cmd_funcs[0]) == 0){	
		/*cd Input*/
		
		sys_cd(params);
		
	} else if(strcmp(coms,cmd_funcs[1]) == 0){ 
		/*set Input command*/
		
		varInsert(params[0],params, ars);
		
	} else if(strcmp(coms,cmd_funcs[2]) == 0){ 
		/*unset Input command*/
		
		varDelete(varFindID(params[0]), ars);
		
	}  else if(strcmp(coms,cmd_funcs[3]) == 0){ 
		/*printlvars Input command*/
		
		varPrint();
		
	} else if(strcmp(coms,cmd_funcs[4]) == 0){
		ret = 0;
		return ret;
	} else if(coms[0] == '$'){
		_state=TEMP;
		if((tmp = varFindData(coms)) != (char**)0){
			cmd_process(tmp);
			_state=NORM;
		}
	} else {
		/*System Calls of UNIX commands using execvp*/
		ret = 2;
	}
	
	return ret;
}

/*-------------------*/
/*The Terminals Loops*/
void cmd_loop(){
	int done = 1;
	char** line;
	
	init_varT();
	while(done){
		
		cmd_create();
		
		line = cmd_parser();

		done = cmd_process(line);
	}
}

/*---------------------*/
/*System Functions*/


void sys_cd(char** args){
	
	if(args[0] == (char*)0){
		printf("cmd: Please select a path for the use of cd command");
	} else {
		if(chdir(args[0]) != 0){
			perror("cmd");
		}
	}
}
