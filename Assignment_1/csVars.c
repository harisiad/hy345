#include "cSsh.h"

int varID = 0;

Variables *varTable[VARLIMIT];

void init_varT(){
	int i;
	
	for(i=0; i<VARLIMIT; i++){
		varTable[i] = varCreate();
	}
}

Variables *varCreate(){

	struct Variables *tmp;
	
	if(varID == VARLIMIT){
		return (Variables*)0;
	}
	
	tmp = (Variables*)malloc(sizeof(Variables));
	if(tmp == (Variables*)0){
		printf("cmd: Couldn't allocate memory for Variable Table. Program is terminating...");
		exit(1);
	}
	
	tmp->name = (char*)0;
	tmp->data = (char**)0;
	tmp->id = 20;
	
	return tmp;
}

void varInsert(char* n, char** d, int cellNum){
	int aval, i;
	
	Variables *tmp;
	
	tmp = varCreate();
	aval = varAvailable();
	
	if((VARLIMIT - varID) == 0){
		printf("-You have reached the limit of\
				\nvariables that can be created.\
				\nFirst delete some so you can create more.\n");
		return;
	}
	
	tmp->name = (char*)malloc(strlen(n)*sizeof(char));
	if(tmp->name == NULL){
		printf("cmd: Failure in allocating memory for field name in struct vars temp. Program terminating...");
		exit(1);
	}
	strcpy(tmp->name,n);
	
	tmp->data = (char**)malloc(cellNum*sizeof(char*) + 1);
	if(tmp->data == NULL){
		printf("cmd: Failure in allocating memory for field data in struct vars temp. Program terminating...");
		exit(1);
	}
	
	if(cellNum%2 == 0){
		for(i=1; i<cellNum; i++){
			tmp->data[i-1] = (char*)malloc((sizeof(char)*strlen(d[i])) + 1*sizeof(char));
			tmp->data[i-1] = d[i];
			
			strcat(tmp->data[i-1],"\0");
		}
	} else {
		for(i=1; i<cellNum-1; i++){
			tmp->data[i-1] = (char*)malloc((sizeof(char)*strlen(d[i])) + 1*sizeof(char));
			tmp->data[i-1] = d[i];
			
			strcat(tmp->data[i-1],"\0");
		}
	}
	
	tmp->id = aval;
	
	varTable[aval] = tmp;
	
	varID++;
	
}

void varDelete(int delID, int cellNum){
	int i;
	
	Variables* tmp, *del;
	
	varID--;
	del = varTable[delID];
	tmp = varCreate();
	
	if((delID<0) || (delID>VARLIMIT-1)){
		printf("Wrong ID number. \
		\nPlease choose a valid ID from 0-19");
		return;
	}
	

	varTable[delID]->name = (char*)0;
	for(i=0; i<cellNum; i++){
		varTable[delID]->data[i] = (char*)0;
	}
	
	varTable[delID] = tmp;
	
	free(del);
	
}

int varAvailable(){
	int i;
	
	for(i=0; i<VARLIMIT; i++){
		if(varTable[i]->id == 20){
			
			return i;
		}
	}
	return -1;
}

int varFindID(char* finName){
	int i;
	
	if(finName == NULL){
		printf("FinName is NULL...\n");
		return -1;
	}
	
	for(i=0; i<VARLIMIT; i++){
		if(varTable[i]->id != 20){
			if(strcmp(varTable[i]->name,finName) == 0){
				return varTable[i]->id;
			}
		}
	}
	return 0;
}

char** varFindData(char* Dname){
	int i;
	char *tmp;
	
	tmp = Dname;
	memmove(tmp, tmp + 1, strlen(tmp));
	
	if(tmp == NULL){
		printf("FinName is NULL...\n");
		return (char**)0;
	}
	
	for(i=0; i<VARLIMIT; i++){
		if(varTable[i]->id != 20){
			if(strcmp(varTable[i]->name,tmp) == 0){
				return varTable[i]->data;
			}
		}
	}
	printf("There is no variable with that name.\
	\n Please use printlvars command to see variable list.");
	return (char**)0;
}

void varPrint(){
	int i, j, count=0;
	printf("ID\tName\tDATA\n\n");
	for(i=0; i<VARLIMIT; i++){
		if((varTable[i]->name != (char*)0) && (varTable[i]->data[0] != (char*)0)){
			printf("%d:\t%s\t",varTable[i]->id,varTable[i]->name);
			j=0;
			while(varTable[i]->data[j] != NULL){
				printf("%s",varTable[i]->data[j++]);
				printf(" ");
			}
			printf("\n");
			count++;
		}
	}
	count = VARLIMIT - count;
	printf("You can create %d more variables.\n",count);
}
/*
int main(){
	int i;
	char **test;
	char *str = "ls -l";
	
	test = (char**)malloc(3*sizeof(char*));
	
	for(i=0; i<2; i++){
		test[i] = (char*)malloc(sizeof(char)*strlen(str));
		strcpy(test[i], str);
	}
	
	
	init_varT();
	for(i=0; i<4; i++){
		
		varInsert("a1",test , 2);
		varInsert("a2",test , 2);
		varInsert("a3",test , 2);
		varInsert("a4",test , 2);
		varInsert("a5",test , 2);
	}
	
	varPrint();
	
	varDelete(varFind("a1"), 2);
	
	varPrint();
	
	varInsert("a1",test , 2);
	
	varPrint();
}*/
