#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>

#define NUMTOKENS 100
#define VARLIMIT 20

#define NODAEMON 0
#define DAEMON 1

#define NORM 0
#define TEMP 1
#define INRED 2
#define OUTREDWR 3
#define OUTREDA 4
#define INOUTRED 5

/*Command Prompt Functions*/

void cmd_create();

char** cmd_parser();

int cmd_process(char** cmd_line);

int cmd_switch(char* coms, char** params, int ars);

void cmd_loop();

/*System Functions*/

void sys_cd(char** args);

	/*Variable List Function*/
typedef struct Variables{
	int id;
	char* name;
	char** data;
	
}Variables;

void init_varT();

Variables* varCreate();

void varInsert(char* n, char** d, int cellNum);

int varAvailable();

void varDelete(int delID, int cellNum);

int varFindID(char* finName);

char** varFindData(char* Dname);

void varPrint();


