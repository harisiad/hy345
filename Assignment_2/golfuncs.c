#include "gol.h"

int genI=0;
int **grid;
Mayor **m;
pthread_mutex_t my_lock;

/*Game Loop*/
/*--------------------------------------------*/
void gameLoop(){
	int i, lTmp;
	void *retval;	
	pthread_t gm[100];	//Grid Master
	
	lTmp = pthread_mutex_init(&my_lock,NULL);
	
	grid = inToGrid();
	
	init_Mayors();
	
	init_MayorsToTowns(grid);
	
	for(i=0; i<100; i++){
		pthread_create(&gm[i], NULL, townManagment, m[i]);
	}
	
	for(i=0; i<100; i++){
		pthread_join(gm[i], &retval);
	}
	
	pthread_mutex_destroy(&my_lock);
}
/*Town Managment*/
/*--------------------------------------------*/

void init_Mayors(){
	int i;
	m = malloc(sizeof(Mayor*)*100);
	for(i=0; i<100; i++){
		m[i] = create_Mayor();
	}
}

Mayor *create_Mayor(){
	Mayor *tmp;
	
	tmp = malloc(sizeof(Mayor));
	if(tmp == (Mayor*)0){
		printf("Couldn't allocate memory for Mayor. Program is terminating...");
		exit(1);
	}
	
	tmp->mid = 0;
	tmp->town = (int**)0;
	tmp->state = 0;
	tmp->population = 0;
	
	return tmp;
}

/* mid configures the town in which a mayor is a mayor of
 * for example mayor with mid = 17 is mayor of town of row 1 column 8
 * and so on
 */
void init_MayorsToTowns(int **g){
	int i;
	int x=0, y=0;

	
	for(i=0; i<100; i++){
		if((i%10 == 0)&&(i != 0)){
			x++;
		}
		m[i] = appointMayor(i, g,x*10,0, x);
	}
}

Mayor *appointMayor(int id, int **g, int s, int p, int sec){
	Mayor *tmp;
	
	tmp = create_Mayor();
	
	tmp->mid = id;
	
	tmp->town = g;
	
	tmp->state = s;
	
	tmp->population = countPop(tmp);
	
	return tmp;
}

int countPop(struct Mayor *m){
	int i,j;
	int tmid;
	int tmpState;
	int count = 0;
	Mayor *tmp = m;

	tmid = tmp->mid % 10;
	tmpState = tmp->state;
	
	for(i=tmpState; i<tmpState+10; i++){
		for(j=tmid*10; j<(tmid+1)*10; j++){
			if(tmp->town[i][j] == 1){
				count++;
			}
		}
	}
	return count;
}

int checkNeig(struct Mayor *m, int x, int y){
	int i,j;
	int bX, lX, bY, lY;
	int countalive = 0;
	Mayor *tmp = m;
	
	if(x==0){
		bX=0;
		lX=1;
		
	} else if(x==99){
		bX=x-1;
		lX=x;
		
	} else {
		bX=x-1;
		lX=x+1;
	}
	
	if(y==0){
		bY=0;
		lY=1;
	} else if (y==99){
		bY=y-1;
		lY=y;
	} else {
		bY=y-1;
		lY=y+1;
	}
	
	for(i=bX; i<=lX; ++i){
		for(j=bY; j<=lY; ++j){
			if(tmp->town[i][j] == 1){
				++countalive;
			}
		}
	}
	
	return countalive-1;
	
}

/*Thread Functions*/
/*--------------------------------------------*/
void *townManagment(void *mi){
	int i,j, count;
	int tmid, tmst;
	Mayor *tmi = mi;
	
	tmid = tmi->mid%10;
	tmst = tmi->state;

	while(1){
		
		pthread_mutex_lock(&my_lock);
		if(signal(SIGINT, sig_han) == SIG_ERR){
			printf("\nCan't catch SIGINT\n");
		}
		
		for(i = tmst; i<tmst+10; i++){
			for(j=tmid*10; j<(tmid+1)*10; j++){
				//printf("%d", tmi->town[i][j]);
				count = checkNeig(tmi,i,j);
				if((tmi->town[i][j] == 0)&&(count == 3)){
					tmi->town[i][j] = 1;
				} else if(tmi->town[i][j] == 1) {
					if((count < 2)||(count > 3)){
						tmi->town[i][j] = 0;
					} else {
						tmi->town[i][j] = 1;
					}
				}
			}
			//printf("\n");
		}
		printf("\nGENERATION: %d\n\n", ++genI);
		printGrid(grid);
		
		pthread_mutex_unlock(&my_lock);
		
	}
	
	
}

/*Signal Handler Function*/
/*--------------------------------------------*/
void sig_han(int signo){
	if(signo == SIGINT){
		printf("Signal SIGINT was recieved by the user.\n The program is exiting...\n");
		exit(1);
	}
}


/*Grid Functions*/
/*--------------------------------------------*/

/*Input into Grid Function\
 * No-arguments
 * gridTmp takes in input
 * return double array grid
 * */
int** inToGrid(){
	int **gridTmp;
	int input, i, j;
	
	gridTmp = (int**)malloc(100*sizeof(int*));
	
	for(i=0; i<100; i++){
		gridTmp[i] = (int*)malloc(100*sizeof(int));
		for(j=0; j<100; j++){
			scanf("%d", &input);
			gridTmp[i][j] = input;
		}
	}
	
	return gridTmp;
}

/*Printing of Grid
 * Visually accessible with x-3 zoom in Terminal
 * */
void printGrid(int **g){
	int i,j,z,w;
	
	for(z=0; z<10; z++){
		
		for(i=z*10; i<(z+1)*10; i++){
			printf("|");
			for(w=0; w<10; w++){
				for(j=w*10; j<(w+1)*10; j++){
					printf("%d|", g[i][j]);
				}
				printf("||");
			}
			printf("\n");
		}
		printf("\n");
	}
}

