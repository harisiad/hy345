#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>
#include <unistd.h>

/* Struct Mayor
 * town -> 10x10 size Grid
 * population -> Number of Alive houses
 * mid -> Master's ID
 * */
typedef struct Mayor{
	int mid;
	int **town;
	int state;
	int population;
}Mayor;

/*Town Managment*/
void init_Mayors();

Mayor *create_Mayor();

void init_MayorsToTowns(int **g);

Mayor *appointMayor(int id, int **g, int s, int p, int sec);

int countPop(struct Mayor *m);

int checkNeig(struct Mayor *m, int x, int y);

/*Game Loop*/
void gameLoop();

/*Thread Function*/
void *townManagment(void *mi);

/*Signal Handler Function*/
void sig_han(int signo);

/*Grid Functions*/
int** inToGrid();

void printGrid(int **g);

